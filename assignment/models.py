from django.db import models
from django.contrib.auth.models import User
from clas.models import Class

# Create your models here.
class Assignment(models.Model):
    #a_id=models.
    uploaded_by=models.ForeignKey(User)
    issue_date=models.DateField(auto_now_add=True)
    url=models.CharField(max_length=100)
    clas = models.ForeignKey(Class)
    end_date=models.DateField()
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.uploaded_by