from django.db import models
from django.contrib.auth.models import User
from college.models import College



# Create your models here.
class Class(models.Model):
	college = models.ForeignKey(College)
	u = models.ForeignKey(User)
	grade = models.CharField(max_length=10)
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.college_id