from django.contrib import admin
from .models import College
class AdminCollage(admin.ModelAdmin):
	list_display = ['name', 'address', 'desc']
	search_fields = list_display
	list_filter = list_display

admin.site.register(College, AdminCollage)

