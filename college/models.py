from django.db import models

# Create your models here.
class College(models.Model):
	name=models.CharField(max_length=50)
	address=models.CharField(max_length=30)
	desc=models.CharField(max_length=200)
	established_date = models.DateField()

	def __str__(self):
		return self.name