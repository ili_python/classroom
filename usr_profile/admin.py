from django.contrib import admin

# Register your models here.
from .models import UserProfile

admin.site.register(UserProfile)#to show in the admin panel
