from django import forms

class Login_form(forms.Form):
    username = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class':'form-control'}))
    #username = forms.CharField(required=false,max_length=20, widget=forms.TextInput(attrs={'class':'form-control'}))
    #username = forms.CharField(label='UserName')
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(Login_form, self).__init__(*args, **kwargs)
        self.fields['password'].widget.attrs["class"] = 'form-control'
        self.fields['password'].widget.attrs["placeholder"] = 'password'

