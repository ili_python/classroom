from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class UserProfile(models.Model):
	#user=models.ForeignKey(User) #one to many
	user=models.OneToOneField(User)#makes the foreign key
	grade=models.CharField(max_length=5)
	address=models.CharField(max_length=50)
	email=models.CharField(max_length=50,blank=True)
	school=models.CharField(max_length=200)
	created_date = models.DateTimeField(auto_now_add=True)
	modified_date = models.DateTimeField(auto_now=True)
	def __str__(self):
		return self.user.first_name or self.user.username
