from django.shortcuts import render
from usr_profile.models import UserProfile
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from forms import Login_form


# Create your views here.
def ListProfile(request):
	profile=UserProfile.objects.all()
	#return HttpResponse(profile)
	data = {'profile_list':profile,
			'user':'sanjay'
			}
	return render(request,'user/profile.html',data)

def Login(request):
    form = Login_form()
    error = ''
    if request.method == "POST":
        form = Login_form(request.POST)
        if form.is_valid():
            username =  form.cleaned_data.get("username")
            password = form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    error = "This user is not active!"
            else:
                error = 'Username or password is invalid!'

    data = {'form' : form, 'error': error}

    return render(request, 'user/login.html',data)

def Logout(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required
def Home_page(request):
    data = {'title': 'My Home Page'}
    return render(request, 'profile.html', data)


def Dashboard(request):
    data = {'title' : 'my dashboard'}
    return render(request, 'user/dashboard.html',data)


